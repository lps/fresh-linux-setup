#!/bin/bash
# Created 16 Feb 2017
# Author Eugeniu Costetchi <costezki.eugen@gmail.com>
# Adding repositories

echo "--------------------------------------------------------------";
echo " Install basic Latex & TexStudio								";
echo "--------------------------------------------------------------";
# sudo apt install -y texlive texstudio texlive-humanities texlive-science texlive-latex-extra texlive-lang-european lmodern texlive-fonts-extra --no-install-recommends
sudo apt install -y texlive texlive-latex-extra texlive-fonts-extra texstudio lmodern --no-install-recommends
sudo apt --purge remove tex.\*-doc$