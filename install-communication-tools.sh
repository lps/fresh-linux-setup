

mkdir ~/temp | true
cd ~/temp

# install MsTeams

curl -L "https://go.microsoft.com/fwlink/p/?LinkID=2112886&clcid=0x409&culture=en-us&country=US" -o msteams.deb
sudo dpkg -i msteams.deb


# install Zoom client

sudo apt install gdebi
wget https://zoom.us/client/latest/zoom_amd64.deb
sudo apt install ./zoom_amd64.deb

# install insync (client for Google Drive)
# follow this guide [here](https://help.insynchq.com/en/articles/2352071-linux-installation-guide)
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys ACCAF35C
sudo echo "deb http://apt.insync.io/ubuntu focal non-free contrib" | sudo tee -a /etc/apt/sources.list.d/insync.list
sudo apt-get update
sudo apt-get install insync

# other tools
sudo apt install flameshot

sudo apt -y install vlc doublecmd-gtk

sudo snap install --classic sublime-text
sudo snap install --classic slack

# sudo snap install --classic pycharm-professional
# sudo snap install --classic webstorm

sudo add-apt-repository ppa:hluk/copyq
sudo apt update
sudo apt -y install copyq
