#!/bin/bash
# Created 11 Jun 2020
# Author Eugeniu Costetchi <costezki.eugen@gmail.com>


mkdir -p ~/temp
cd ~/temp


echo "Download latest Oxygen Editor"
wget -nc https://mirror.oxygenxml.com/InstData/Editor/Linux64/VM/oxygen-64bit-openjdk.sh

bash ./oxygen-64bit-openjdk.sh
