#!/bin/bash

mkdir ~/temp
cd ~/temp

wget -nc https://dl.winehq.org/wine-builds/winehq.key && sudo apt-key add winehq.key

#echo "Assuming Ubuntu 18.04 distribution"
#sudo apt-add-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ bionic main'

#echo "Assuming Ubuntu 19.04 distribution"
#sudo apt-add-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ disco main'

#echo "Assuming Ubuntu 19.10 distribution"
#sudo apt-add-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ eoan main'

echo "Assuming Ubuntu 20.04 distribution"
sudo add-apt-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ focal main'

echo "adding a repository where to find libfaudio0 package"
sudo add-apt-repository ppa:cybermax-dexter/sdl2-backport
sudo apt-get update

sudo dpkg --add-architecture i386

sudo apt-get install --install-recommends winehq-devel
sudo apt-get install winetricks fonts-crosextra-carlito playonlinux

# Delete any previously installed Wine configurations; WARNING use with care
# sudo rm -rf .wine/ .local/share/wineprefixes/

winecfg

winetricks msxml3
winetricks msxml4
winetricks msxml6
winetricks mdac28
winetricks jet40
winetricks -q msxml3 msxml4 msxml6 mdac28 jet40

winetricks --force mdac28


# wget https://sparxsystems.com/securedownloads/easetupfull15.msi
# wget https://www.sparxsystems.com/securedownloads/easetupfull151.msi

#echo "Insalling the EA now"
#wine msiexec /i easetupfull15.msi
