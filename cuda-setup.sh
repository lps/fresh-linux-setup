#!/bin/bash
# Created 31 May 2020
# Author Eugeniu Costetchi <costezki.eugen@gmail.com>

echo "Checking the graphic cards"
lspci | grep -i nvidia

echo "Checking the system"
uname -m && cat /etc/*release

echo "Checking the GCC compiler"
gcc --version

echo "Installing the CUDA toolkit from the Ubuntu repository"
sudo apt update
sudo apt install nvidia-cuda-toolkit
nvcc --version

sudo apt install g++ cmake gfortran