#!/bin/bash
# Created 20 Jul 2022
# Author Eugeniu Costetchi <costezki.eugen@gmail.com>


sudo apt install -y software-properties-common
sudo add-apt-repository -y ppa:deadsnakes/ppa

sudo apt install -y python3.7 python3.7-distutils python3.7-venv
sudo apt install -y python3.8 python3.8-distutils python3.8-venv
sudo apt install -y python3.9 python3.9-distutils python3.9-venv
sudo apt install -y python3.10 python3.10-distutils python3.10-venv
sudo apt install -y python3.11 python3.11-distutils python3.11-venv

# sudo apt install python3-distutils python3-pip python3-venv python3-pip

sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.7 1
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.8 2
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.9 3
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.10 4
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.11 5

sudo update-alternatives --config python